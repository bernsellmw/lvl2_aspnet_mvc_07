﻿using System.Web.UI.WebControls;

namespace LVL2_ASPNet_MVC_07.Controllers
{
    internal class ReportViewer
    {
        public object ProcessingMode { get; internal set; }
        public object ServerReport { get; internal set; }
        public Unit Width { get; internal set; }
    }
}